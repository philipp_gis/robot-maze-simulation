# Robot Maze Simulation

![Visualisation](/images/visualisation.png)

## Requirements
Simulation of a robot that does not know its surroundings. With the help of distance sensors, the robot has to find 3 positions and the exit in a maze. It must travel through the positions in the correct order and then go to the exit.

## Approach
- only readable autonomous distance sensores
- repeatable set of commands for the robot
- ability to create a map from the measured values of the distance sensors
- recognise significant locations such as doors
- dynamic generation of a navigation mesh from the measured map and the significant locations
- moving throgh the navigation mesh with a a-star algorithm

## Goals
Learning the basics of Robotics

## Getting Started

## Future Work
- remake project files in Unity
- optimize perfomance
- add sensor noice
