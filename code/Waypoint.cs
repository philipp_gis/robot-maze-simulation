using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{
    public enum type { TBD, X, TN, TE, TS, TW, LNW, LSE, LSW, LNE, IN, IE, IS, IW }

    public enum state { TBD, WALL, WAY }
    // Weltposition des Rasterelementes
    [SerializeField] private Vector2 worldPosition;

    // Ist der Wegpunkt veraltet
    [SerializeField] private bool trash;

    [Header("")]
    // In welche Richtungen existieren Transitionen
    [SerializeField] private type waypoint_Type = type.TBD;

    [Header("")]
    // In welche Richtungen existieren Transitionen
    [SerializeField] private state state_N = state.TBD;
    [SerializeField] private state state_E = state.TBD;
    [SerializeField] private state state_S = state.TBD;
    [SerializeField] private state state_W = state.TBD;

    // Referenzen auf die Wegpunkte
    [SerializeField] private GameObject reference_N;
    [SerializeField] private GameObject reference_E;
    [SerializeField] private GameObject reference_S;
    [SerializeField] private GameObject reference_W;

    // A-Stern Attribute
    public enum a_star { OPEN, CLOSED }
    public enum turnside { TBD, LEFT, RIGHT }
    [SerializeField] private a_star a_star_state = a_star.OPEN;
    [SerializeField] private GameObject predecessor;
    [SerializeField] private float angle = 0;
    [SerializeField] private turnside current_turnside = turnside.TBD;
    [SerializeField] private float start_Cost = 0;
    [SerializeField] private float end_Cost = 0;
    [SerializeField] private float total_Cost = 0;


    public Vector2 WorldPosition { get => worldPosition; set => worldPosition = value; }
    public bool Trash { get => trash; set => trash = value; }
    public state State_N { get => state_N; set => state_N = value; }
    public state State_E { get => state_E; set => state_E = value; }
    public state State_S { get => state_S; set => state_S = value; }
    public state State_W { get => state_W; set => state_W = value; }
    public GameObject Reference_N { get => reference_N; set => reference_N = value; }
    public GameObject Reference_E { get => reference_E; set => reference_E = value; }
    public GameObject Reference_S { get => reference_S; set => reference_S = value; }
    public GameObject Reference_W { get => reference_W; set => reference_W = value; }
    public type Waypoint_Type { get => waypoint_Type; set => waypoint_Type = value; }
    public a_star A_star_state { get => a_star_state; set => a_star_state = value; }
    public GameObject Predecessor { get => predecessor; set => predecessor = value; }
    public float Start_Cost { get => start_Cost; set => start_Cost = value; }
    public float End_Cost { get => end_Cost; set => end_Cost = value; }
    public float Total_Cost { get => total_Cost; set => total_Cost = value; }
    public float Angle { get => angle; set => angle = value; }
    public turnside Current_Turnside { get => current_turnside; set => current_turnside = value; }
}
