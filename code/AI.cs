using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour
{
    private Data data;
    private Sensors sensor;

    public enum Robot_States { SCAN_AREA, FIND_NEAREST_WAYPOINT, GO_TO_NEAREST_WAYPOINT, FIND_PATH_TO_NEXT_WAYPOINT, GO_TO_NEXT_WAYPOINT, DRIVE_IN_OPEN_DIRECTION, DEFAULT }
    public Robot_States current_State;
    public enum Pathplanning { START_TURN, END_TURN, MOVE };
    public Pathplanning path_state = Pathplanning.START_TURN;

    Vector3 nearest_Waypoint_Position;
    GameObject nearest_Waypoint;
    public List<GameObject> open_List;
    public List<GameObject> closed_List;
    public List<GameObject> path_list;

    int path_count;
    float open_Direction;

    private void Awake()
    {
        data = GetComponent<Data>();
        sensor = GetComponent<Sensors>();

        current_State = Robot_States.SCAN_AREA;
        target_Rotation = new Vector3(this.transform.eulerAngles.x, this.transform.eulerAngles.y - 90);
    }

    private void Update()
    {
        Robot_AI();
    }

    private void Robot_AI()
    {
        if (current_State == Robot_States.SCAN_AREA)
        {
            Scan_Aera();
        }

        if (current_State == Robot_States.FIND_NEAREST_WAYPOINT)
        {
            Find_Nearest_Waypoint();
        }

        if (current_State == Robot_States.GO_TO_NEAREST_WAYPOINT)
        {
            Go_To_Nearest_Waypoint();
        }

        if (current_State == Robot_States.FIND_PATH_TO_NEXT_WAYPOINT)
        {
            a_Star();
        }

        if (current_State == Robot_States.GO_TO_NEXT_WAYPOINT)
        {
            Go_To_Next_Waypoint();
        }

        if (current_State == Robot_States.DRIVE_IN_OPEN_DIRECTION)
        {
            Drive_in_Open_Direction();
        }
    }

    #region Scan Area
    bool scan_area = true;
    Vector3 target_Rotation = new Vector3();

    private void Scan_Aera()
    {
        #region Aktualisiert die Rotation des Roboters jeden Frame
        Vector3 current_Rotation = this.transform.eulerAngles;
        #endregion

        #region Setzt die gewünschte Zielrotation fest
        if (scan_area)
        {
            target_Rotation = new Vector3(current_Rotation.x, current_Rotation.y + -45, current_Rotation.z);
            scan_area = false;
        }
        #endregion

        #region Dreht Roboter bis Zielrotation erreicht wurde und wechselt in nächsten Zustand
        if (Vector3.Distance(current_Rotation, target_Rotation) > 0.8f)
        {
            this.transform.Rotate(Vector3.down, 0.75f);
        }
        else
        {
            current_State = Robot_States.FIND_NEAREST_WAYPOINT;
            scan_area = true;

            Debug.Log("Scan abgeschlossen");
            Debug.Log("Zustandswechsel zu: " + current_State);
            Debug.Log("");
        }
        #endregion
    }
    #endregion

    #region Find Nearest Waypoint
    private void Find_Nearest_Waypoint()
    {
        #region Initialisierungen
        Vector3 current_Position = this.transform.position;

        float min_30_x = current_Position.x - 3.0f;
        float min_30_y = current_Position.z - 3.0f;
        float max_30_x = current_Position.x + 3.0f;
        float max_30_y = current_Position.z + 3.0f;
        #endregion

        nearest_Waypoint = null;
        foreach (GameObject w in data.WaypointList)
        {
            #region Zugriffsverkürzung
            Vector3 w_Position = new Vector3(w.GetComponent<Waypoint>().WorldPosition.x, -0.25f, w.GetComponent<Waypoint>().WorldPosition.y);
            #endregion

            #region Suche in 3 Meter Radius den naheliegensten Wegpunkt
            if (w_Position.x > min_30_x && w_Position.x < max_30_x && w_Position.z > min_30_y && w_Position.z < max_30_y)
            {
                #region Setze ersten Wegpunkt als günstigsten
                if (nearest_Waypoint == null)
                {
                    nearest_Waypoint = w;
                    nearest_Waypoint_Position = w_Position;
                }
                #endregion

                #region Wenn Distanz kleiner, neuer naheligenster Wegpunkt
                else if (Vector3.Distance(current_Position, w_Position) < Vector3.Distance(current_Position, nearest_Waypoint_Position))
                {
                    nearest_Waypoint = w;
                    nearest_Waypoint_Position = w_Position;
                }
                #endregion
            }
        }
        #endregion

        #region Rechne Drehwinkel aus und geh in den nächsten Zustand
        target_Rotation = new Vector3(this.transform.eulerAngles.x, (this.transform.eulerAngles.y + 360 - Vector3.Angle(nearest_Waypoint_Position - this.transform.position, this.transform.forward)) % 360, this.transform.eulerAngles.z);
        Debug.Log("Naheligenster Punkt ist: " + nearest_Waypoint.GetComponent<Waypoint>().WorldPosition);
        Debug.Log("Mit der Distanz: " + Vector3.Distance(current_Position, nearest_Waypoint_Position));
        Debug.Log("Und der Zielrotation: " + target_Rotation);
        current_State = Robot_States.GO_TO_NEAREST_WAYPOINT;

        Debug.Log("Zustandswechsel zu: " + current_State);
        Debug.Log("");
        #endregion
    }
    #endregion

    #region Go To Nearest Waypoint
    private void Go_To_Nearest_Waypoint()
    {
        #region Aktualisiert Position und Rotation jeden Frame
        Vector3 current_Rotation = this.transform.eulerAngles;
        Vector3 current_Position = this.transform.position;
        #endregion

        if (Vector3.Distance(current_Rotation, target_Rotation) > 0.8f)
        {
            this.transform.Rotate(Vector3.down, 0.75f);
        }
        else
        {
            if (Vector3.Distance(current_Position, nearest_Waypoint_Position) > 0.05f)
            {
                this.transform.Translate(Vector3.forward * 0.01f);
            }
            else
            {
                current_State = Robot_States.FIND_PATH_TO_NEXT_WAYPOINT;

                Debug.Log("Fahrt abgeschlossen");
                Debug.Log("Zustandswechsel zu: " + current_State);
                Debug.Log("");
            }
        }
    }
    #endregion

    private void a_Star()
    {
        #region Endpunkt festlegen
        // Offenen Knoten finden und als Endpunkt festlegen
        GameObject end_point = null;
        foreach (GameObject w in data.WaypointList)
        {
            Waypoint waypoint_w = w.GetComponent<Waypoint>();

            if (waypoint_w.State_N == Waypoint.state.WAY && waypoint_w.Reference_N == null)
            {
                Debug.Log("Endpunkt: " + waypoint_w.WorldPosition);
                end_point = w;
                open_Direction = 0;
                break;
            }

            else if (waypoint_w.State_E == Waypoint.state.WAY && waypoint_w.Reference_E == null)
            {
                Debug.Log("Endpunkt: " + waypoint_w.WorldPosition);
                end_point = w;
                open_Direction = 90;
                break;
            }

            else if (waypoint_w.State_S == Waypoint.state.WAY && waypoint_w.Reference_S == null)
            {
                Debug.Log("Endpunkt: " + waypoint_w.WorldPosition);
                end_point = w;
                open_Direction = 180;
                break;
            }

            else if (waypoint_w.State_W == Waypoint.state.WAY && waypoint_w.Reference_W == null)
            {
                Debug.Log("Endpunkt: " + waypoint_w.WorldPosition);
                end_point = w;
                open_Direction = 270;
                break;
            }
        }
        #endregion

        #region Startpunkt festlegen
        // Startpunkt Berechnen, in Offene Liste ablegen und als aktueller Pungt setzen
        GameObject start_point = nearest_Waypoint;
        #endregion

        #region Startpunkt berechnen
        Waypoint start_point_w = start_point.GetComponent<Waypoint>();
        Waypoint end_point_w = end_point.GetComponent<Waypoint>();
        Debug.Log("Startpunkt: " + start_point_w.WorldPosition);

        start_point_w.Angle = this.transform.eulerAngles.y;
        start_point_w.Current_Turnside = Waypoint.turnside.LEFT;
        start_point_w.End_Cost = Vector2.Distance(end_point_w.WorldPosition, start_point_w.WorldPosition);
        start_point_w.Total_Cost = start_point_w.Start_Cost + start_point_w.End_Cost;
        #endregion

        #region Startpunkt in Openlist einfügen
        open_List = new List<GameObject> { };
        closed_List = new List<GameObject> { };
        Debug.Log("wird Openlist eingefügt " + start_point_w.WorldPosition);
        open_List.Add(start_point);
        #endregion

        #region Startpunkt wird zum aktuell zu prüfenden Punkt
        GameObject current_point = start_point;
        #endregion

        #region Wegfindung
        int p = 0;
        while (current_State == Robot_States.FIND_PATH_TO_NEXT_WAYPOINT && current_point != end_point)
        {
            #region Berechne Nachbarn des aktuellen Punktes
            Waypoint current_point_w = current_point.GetComponent<Waypoint>();

            // Gehe Nachbarn vom aktuellen Punkt durch und berechne die Endpunkt, Startpunkt und Gesamtkosten

            #region Nordnachbar berechnen
            // Nordnachbar

            if (current_point_w.State_N == Waypoint.state.WAY && current_point_w.Reference_N && current_point_w.Reference_N.GetComponent<Waypoint>().A_star_state == Waypoint.a_star.OPEN)
            {
                Waypoint current_point_N = current_point_w.Reference_N.GetComponent<Waypoint>();
                Debug.Log("nordnachbar " + current_point_N.WorldPosition);
                // Vorgängerkosten + Wegkosten (1.4s pro Meter) + Drehkosten (0.02244s pro Grad)

                #region Vorgänger Wegkosten
                float predecessor_Cost = current_point_w.Start_Cost;
                #endregion

                #region Bewegungskosten
                float move_Cost = Vector2.Distance(current_point_w.WorldPosition, current_point_N.WorldPosition) * 1.4f;
                #endregion

                #region Drehkosten + Winkel des Nachbarn
                float turn_Cost = 0f;

                float tmp_angle = 0;

                if (current_point_w.Angle < 0)
                {
                    turn_Cost = (0 - current_point_w.Angle) * 0.02244f;
                    current_point_N.Current_Turnside = Waypoint.turnside.RIGHT;
                }
                else
                {
                    turn_Cost = (0 - current_point_w.Angle) * 0.02244f * (-1);
                    current_point_N.Current_Turnside = Waypoint.turnside.LEFT;
                }
                #endregion

                #region Temporäre Berechnung der Kosten

                #region Startkosten
                float tmp_startcost = predecessor_Cost + move_Cost + turn_Cost;
                #endregion

                #region Endkosten
                float tmp_endcost = Vector2.Distance(end_point_w.WorldPosition, current_point_N.WorldPosition);
                #endregion

                #region Gesamtkosten
                float tmp_totalcost = tmp_startcost + tmp_endcost;
                #endregion

                #endregion

                #region Punkt wurde schon mal besucht
                bool new_openpoint = true;
                foreach (GameObject w in open_List)
                {
                    #region Wenn aktueller Punkt kleinere Kosten hat -> Echte Kosten = Temporäre Kosten
                    if (tmp_totalcost <= current_point_N.Total_Cost)
                    {
                        #region Winkel wird gesetzt
                        current_point_N.Angle = tmp_angle;
                        #endregion

                        #region Startkosten werden gesetzt
                        current_point_N.Start_Cost = tmp_startcost;
                        #endregion

                        #region Endkosten werden gesetzt
                        current_point_N.End_Cost = tmp_endcost;
                        #endregion

                        #region Gesamtkosten werden gesetzz
                        current_point_N.Total_Cost = tmp_totalcost;
                        #endregion

                        #region Vorgänger wird gesetzt
                        current_point_N.Predecessor = current_point;
                        #endregion
                    }
                    #endregion

                    if (current_point_N == w)
                    {
                        new_openpoint = false;
                    }
                }
                #endregion

                #region neuer Punkt
                if (new_openpoint)
                {
                    #region Winkel, Start, End, Gesamtkosten und Vorgänger wird gesetzt
                    current_point_N.Angle = tmp_angle;
                    current_point_N.Start_Cost = tmp_startcost;
                    current_point_N.End_Cost = tmp_endcost;
                    current_point_N.Total_Cost = tmp_totalcost;
                    current_point_N.Predecessor = current_point;
                    #endregion

                    #region Neuer Punkt wird in die Openlist aufgenommen
                    open_List.Add(current_point_N.gameObject);
                    #endregion
                }
                #endregion
            }
            #endregion

            #region Ostnachbar berechnen
            // Ostnachbar
            if (current_point_w.State_E == Waypoint.state.WAY && current_point_w.Reference_E && current_point_w.Reference_E.GetComponent<Waypoint>().A_star_state == Waypoint.a_star.OPEN)
            {
                Waypoint current_point_E = current_point_w.Reference_E.GetComponent<Waypoint>();
                Debug.Log("ostnachbar " + current_point_E.WorldPosition);

                // Vorgängerkosten + Wegkosten (1.4s pro Meter) + Drehkosten (0.02244s pro Grad)
                float predecessor_Cost = current_point_w.Start_Cost;
                float move_Cost = Vector2.Distance(current_point_w.WorldPosition, current_point_E.WorldPosition) * 1.4f;
                float turn_Cost = 0f;

                float tmp_angle = 90;

                if (current_point_w.Angle < 90)
                {
                    turn_Cost = (90 - current_point_w.Angle) * 0.02244f;
                    current_point_E.Current_Turnside = Waypoint.turnside.RIGHT;
                }
                else
                {
                    turn_Cost = (90 - current_point_w.Angle) * 0.02244f * (-1);
                    current_point_E.Current_Turnside = Waypoint.turnside.LEFT;
                }

                float tmp_startcost = predecessor_Cost + move_Cost + turn_Cost;
                float tmp_endcost = Vector2.Distance(end_point_w.WorldPosition, current_point_E.WorldPosition);
                float tmp_totalcost = tmp_startcost + tmp_endcost;

                #region Punkt wurde schon mal besucht
                bool new_openpoint = true;
                foreach (GameObject w in open_List)
                {
                    #region Wenn aktueller Punkt kleinere Kosten hat -> Echte Kosten = Temporäre Kosten
                    if (tmp_totalcost <= current_point_E.Total_Cost)
                    {
                        #region Winkel wird gesetzt
                        current_point_E.Angle = tmp_angle;
                        #endregion

                        #region Startkosten werden gesetzt
                        current_point_E.Start_Cost = tmp_startcost;
                        #endregion

                        #region Endkosten werden gesetzt
                        current_point_E.End_Cost = tmp_endcost;
                        #endregion

                        #region Gesamtkosten werden gesetzz
                        current_point_E.Total_Cost = tmp_totalcost;
                        #endregion

                        #region Vorgänger wird gesetzt
                        current_point_E.Predecessor = current_point;
                        #endregion
                    }
                    #endregion

                    if (current_point_E == w)
                    {
                        new_openpoint = false;
                    }
                }
                #endregion

                #region neuer Punkt
                if (new_openpoint)
                {
                    #region Winkel, Start, End, Gesamtkosten und Vorgänger wird gesetzt
                    current_point_E.Angle = tmp_angle;
                    current_point_E.Start_Cost = tmp_startcost;
                    current_point_E.End_Cost = tmp_endcost;
                    current_point_E.Total_Cost = tmp_totalcost;
                    current_point_E.Predecessor = current_point;
                    #endregion

                    #region Neuer Punkt wird in die Openlist aufgenommen
                    open_List.Add(current_point_E.gameObject);
                    #endregion
                }
                #endregion

            }
            #endregion

            #region Südnachbar berechnen
            // Südnachbar
            if (current_point_w.State_S == Waypoint.state.WAY && current_point_w.Reference_S && current_point_w.Reference_S.GetComponent<Waypoint>().A_star_state == Waypoint.a_star.OPEN)
            {
                Waypoint current_point_S = current_point_w.Reference_S.GetComponent<Waypoint>();
                Debug.Log("südnachbar " + current_point_S.WorldPosition);

                // Vorgängerkosten + Wegkosten (1.4s pro Meter) + Drehkosten (0.02244s pro Grad)
                float predecessor_Cost = current_point_w.Start_Cost;
                float move_Cost = Vector2.Distance(current_point_w.WorldPosition, current_point_S.WorldPosition) * 1.4f;
                float turn_Cost = 0f;

                float tmp_angle = 180;

                if (current_point_w.Angle < 180)
                {
                    turn_Cost = (180 - current_point_w.Angle) * 0.02244f;
                    current_point_S.Current_Turnside = Waypoint.turnside.RIGHT;
                }
                else
                {
                    turn_Cost = (180 - current_point_w.Angle) * 0.02244f * (-1);
                    current_point_S.Current_Turnside = Waypoint.turnside.LEFT;
                }

                float tmp_startcost = predecessor_Cost + move_Cost + turn_Cost;
                float tmp_endcost = Vector2.Distance(end_point_w.WorldPosition, current_point_S.WorldPosition);
                float tmp_totalcost = tmp_startcost + tmp_endcost;

                #region Punkt wurde schon mal besucht
                bool new_openpoint = true;
                foreach (GameObject w in open_List)
                {
                    #region Wenn aktueller Punkt kleinere Kosten hat -> Echte Kosten = Temporäre Kosten
                    if (tmp_totalcost <= current_point_S.Total_Cost)
                    {
                        #region Winkel wird gesetzt
                        current_point_S.Angle = tmp_angle;
                        #endregion

                        #region Startkosten werden gesetzt
                        current_point_S.Start_Cost = tmp_startcost;
                        #endregion

                        #region Endkosten werden gesetzt
                        current_point_S.End_Cost = tmp_endcost;
                        #endregion

                        #region Gesamtkosten werden gesetzz
                        current_point_S.Total_Cost = tmp_totalcost;
                        #endregion

                        #region Vorgänger wird gesetzt
                        current_point_S.Predecessor = current_point;
                        #endregion
                    }
                    #endregion

                    if (current_point_S == w)
                    {
                        new_openpoint = false;
                    }
                }
                #endregion

                #region neuer Punkt
                if (new_openpoint)
                {
                    #region Winkel, Start, End, Gesamtkosten und Vorgänger wird gesetzt
                    current_point_S.Angle = tmp_angle;
                    current_point_S.Start_Cost = tmp_startcost;
                    current_point_S.End_Cost = tmp_endcost;
                    current_point_S.Total_Cost = tmp_totalcost;
                    current_point_S.Predecessor = current_point;
                    #endregion

                    #region Neuer Punkt wird in die Openlist aufgenommen
                    open_List.Add(current_point_S.gameObject);
                    #endregion
                }
                #endregion
            }
            #endregion

            #region Westnachbar berechnen
            // Westnachbar
            if (current_point_w.State_W == Waypoint.state.WAY && current_point_w.Reference_W && current_point_w.Reference_W.GetComponent<Waypoint>().A_star_state == Waypoint.a_star.OPEN)
            {
                Waypoint current_point_W = current_point_w.Reference_W.GetComponent<Waypoint>();
                Debug.Log("westnachbar " + current_point_W.WorldPosition);

                // Vorgängerkosten + Wegkosten (1.4s pro Meter) + Drehkosten (0.02244s pro Grad)
                float predecessor_Cost = current_point_w.Start_Cost;
                float move_Cost = Vector2.Distance(current_point_w.WorldPosition, current_point_W.WorldPosition) * 1.4f;
                float turn_Cost = 0f;

                float tmp_angle = 270;

                if (current_point_w.Angle < 270)
                {
                    turn_Cost = (270 - current_point_w.Angle) * 0.02244f;
                    current_point_W.Current_Turnside = Waypoint.turnside.RIGHT;
                }
                else
                {
                    turn_Cost = (270 - current_point_w.Angle) * 0.02244f * (-1);
                    current_point_W.Current_Turnside = Waypoint.turnside.LEFT;
                }

                float tmp_startcost = predecessor_Cost + move_Cost + turn_Cost;
                float tmp_endcost = Vector2.Distance(end_point_w.WorldPosition, current_point_W.WorldPosition);
                float tmp_totalcost = tmp_startcost + tmp_endcost;

                #region Punkt wurde schon mal besucht
                bool new_openpoint = true;
                foreach (GameObject w in open_List)
                {
                    #region Wenn aktueller Punkt kleinere Kosten hat -> Echte Kosten = Temporäre Kosten
                    if (tmp_totalcost <= current_point_W.Total_Cost)
                    {
                        #region Winkel wird gesetzt
                        current_point_W.Angle = tmp_angle;
                        #endregion

                        #region Startkosten werden gesetzt
                        current_point_W.Start_Cost = tmp_startcost;
                        #endregion

                        #region Endkosten werden gesetzt
                        current_point_W.End_Cost = tmp_endcost;
                        #endregion

                        #region Gesamtkosten werden gesetzz
                        current_point_W.Total_Cost = tmp_totalcost;
                        #endregion

                        #region Vorgänger wird gesetzt
                        current_point_W.Predecessor = current_point;
                        #endregion
                    }
                    #endregion

                    if (current_point_W == w)
                    {
                        new_openpoint = false;
                    }
                }
                #endregion

                #region neuer Punkt
                if (new_openpoint)
                {
                    #region Winkel, Start, End, Gesamtkosten und Vorgänger wird gesetzt
                    current_point_W.Angle = tmp_angle;
                    current_point_W.Start_Cost = tmp_startcost;
                    current_point_W.End_Cost = tmp_endcost;
                    current_point_W.Total_Cost = tmp_totalcost;
                    current_point_W.Predecessor = current_point;
                    #endregion

                    #region Neuer Punkt wird in die Openlist aufgenommen
                    open_List.Add(current_point_W.gameObject);
                    #endregion
                }
                #endregion
            }
            #endregion

            #endregion

            #region Schiebe überprüften aktuellen Punkt aus der openlist in die closedlist
            open_List.Remove(current_point);
            current_point_w.A_star_state = Waypoint.a_star.CLOSED;


            closed_List.Add(current_point);
            #endregion

            #region Suche nächsten Punkt zum überprüfen mit den geringsten Kosten
            GameObject new_current_point = null;
            Waypoint new_current_point_w = null;
            if (open_List.Count > 0)
            {
                foreach (GameObject w in open_List)
                {
                    #region Zugriffsverkürzung
                    Waypoint w_w = w.GetComponent<Waypoint>();
                    #endregion

                    #region Setze ersten Wegpunkt als günstigsten
                    if (new_current_point == null)
                    {
                        new_current_point = w;
                        new_current_point_w = new_current_point.GetComponent<Waypoint>();
                    }
                    #endregion

                    #region Wenn Kosten günstiger des nächsten Punktes, setze diesen als günstigsten Punkt
                    else if (w_w.Total_Cost <= new_current_point_w.Total_Cost && w_w.End_Cost < new_current_point_w.End_Cost)
                    {
                        new_current_point = w;
                        new_current_point_w = new_current_point.GetComponent<Waypoint>();
                    }
                    #endregion
                }
            }
            else
            {
                Debug.Log("Openlist ist leer!");
            }

            #region Setze günstigsten Punkt als den nächsten zu prüfenden Punkt
            current_point = new_current_point;
            current_point_w = current_point.GetComponent<Waypoint>();
            Debug.Log("");
            Debug.Log(current_point_w.WorldPosition + " neuer Openpunkt");
            #endregion
            #endregion 
            p++;
            if (p == 10)
            {
                Debug.Log("Endlosschleife!");
                break;
            }
        }

        path_list = new List<GameObject> { };

        p = 0;
        #region Weg in einer Liste festlegen
        while (current_point != start_point)
        {
            Debug.Log(current_point.GetComponent<Waypoint>().WorldPosition);
            path_list.Add(current_point);
            current_point = current_point.GetComponent<Waypoint>().Predecessor;
            p++;
            if (p == 10)
            {
                Debug.Log("Endlosschleife!");
                break;
            }
            Debug.Log("");
        }
        #endregion
        #endregion
        path_count = path_list.Count - 1;
        current_State = Robot_States.GO_TO_NEXT_WAYPOINT;

        Debug.Log("Zustandswechsel zu: " + current_State);
        Debug.Log("");
    }

    private void Go_To_Next_Waypoint()
    {
        #region Drehe Roboter in Richtung erster Wegpunkt

        if (path_state == Pathplanning.START_TURN)
        {
            if (nearest_Waypoint.GetComponent<Waypoint>().Current_Turnside == Waypoint.turnside.LEFT)
            {
                Vector3 current_Rotation = this.transform.eulerAngles;
                target_Rotation = new Vector3(this.transform.eulerAngles.x, nearest_Waypoint.GetComponent<Waypoint>().Angle, this.transform.eulerAngles.z);

                if (Vector3.Distance(current_Rotation, target_Rotation) > 0.8f)
                {
                    this.transform.Rotate(Vector3.up, 0.75f);
                }
                else
                {
                    path_state = Pathplanning.MOVE;
                }
            }
            else if (nearest_Waypoint.GetComponent<Waypoint>().Current_Turnside == Waypoint.turnside.RIGHT)
            {
                Vector3 current_Rotation = this.transform.eulerAngles;
                target_Rotation = new Vector3(this.transform.eulerAngles.x, nearest_Waypoint.GetComponent<Waypoint>().Angle, this.transform.eulerAngles.z);

                if (Vector3.Distance(current_Rotation, target_Rotation) > 0.8f)
                {
                    this.transform.Rotate(Vector3.down, 0.75f);
                }
                else
                {
                    path_state = Pathplanning.MOVE;
                }
            }
        }
        #endregion


        #region Drehe dich zum Wegpunkt und fahre hin bis Wegliste durchgearbeitet ist
        else if (path_state == Pathplanning.MOVE)
        {
            if (path_count > -1)
            {
                if (path_list[path_count].GetComponent<Waypoint>().Current_Turnside == Waypoint.turnside.RIGHT)
                {
                    Vector3 current_Rotation = this.transform.eulerAngles;
                    Vector3 current_Position = this.transform.position;

                    target_Rotation = new Vector3(this.transform.eulerAngles.x, path_list[path_count].GetComponent<Waypoint>().Angle, this.transform.eulerAngles.z);
                    Vector3 target_Position = new Vector3(path_list[path_count].GetComponent<Waypoint>().WorldPosition.x, -0.25f, path_list[path_count].GetComponent<Waypoint>().WorldPosition.y);

                    if (Vector3.Distance(current_Rotation, target_Rotation) > 0.8f)
                    {
                        this.transform.Rotate(Vector3.down, 0.75f);
                    }
                    else
                    {
                        if (Vector3.Distance(current_Position, target_Position) > 0.1f)
                        {
                            this.transform.Translate(Vector3.forward * 0.01f);
                        }
                        else
                        {
                            path_count--;
                        }
                    }
                }

                else if (path_list[path_count].GetComponent<Waypoint>().Current_Turnside == Waypoint.turnside.LEFT)
                {
                    Vector3 current_Rotation = this.transform.eulerAngles;
                    Vector3 current_Position = this.transform.position;

                    target_Rotation = new Vector3(this.transform.eulerAngles.x, path_list[path_count].GetComponent<Waypoint>().Angle, this.transform.eulerAngles.z);
                    Vector3 target_Position = new Vector3(path_list[path_count].GetComponent<Waypoint>().WorldPosition.x, -0.25f, path_list[path_count].GetComponent<Waypoint>().WorldPosition.y);

                    if (Vector3.Distance(current_Rotation, target_Rotation) > 0.8f)
                    {
                        this.transform.Rotate(Vector3.up, 0.75f);
                    }
                    else
                    {
                        if (Vector3.Distance(current_Position, target_Position) > 0.1f)
                        {
                            this.transform.Translate(Vector3.forward * 0.01f);
                        }
                        else
                        {
                            path_count--;
                        }
                    }
                } 
            }

            else if (path_count < 0)
            {
                path_state = Pathplanning.END_TURN;
            }
        }
        #endregion

        if (path_state == Pathplanning.END_TURN)
        {
            if (this.transform.eulerAngles.y < open_Direction)
            {
                Vector3 current_Rotation = this.transform.eulerAngles;
                target_Rotation = new Vector3(this.transform.eulerAngles.x, open_Direction, this.transform.eulerAngles.z);

                if (Vector3.Distance(current_Rotation, target_Rotation) > 0.8f)
                {
                    this.transform.Rotate(Vector3.up, 0.75f);
                }
                else
                {
                    current_State = Robot_States.DRIVE_IN_OPEN_DIRECTION;

                    Debug.Log("Zustandswechsel zu: " + current_State);
                    Debug.Log("");
                }
            }
            else if (this.transform.eulerAngles.y > open_Direction)
            {
                Vector3 current_Rotation = this.transform.eulerAngles;
                target_Rotation = new Vector3(this.transform.eulerAngles.x, open_Direction, this.transform.eulerAngles.z);

                if (Vector3.Distance(current_Rotation, target_Rotation) > 0.8f)
                {
                    this.transform.Rotate(Vector3.down, 0.75f);
                }
                else
                {
                    current_State = Robot_States.DRIVE_IN_OPEN_DIRECTION;

                    Debug.Log("Zustandswechsel zu: " + current_State);
                    Debug.Log("");
                }
            }
        }
    }

    void Drive_in_Open_Direction()
    {
        if (sensor.N_Distance > 0.3f)
        {
            this.transform.Translate(Vector3.forward * 0.01f);
        }
        else
        {
            current_State = Robot_States.SCAN_AREA;

            Debug.Log("Zustandswechsel zu: " + current_State);
            Debug.Log("");
        }
    }
}
