using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    public enum nodeType { UNKNOWN, WALL, FREE, GAMEBIT, WAYPOINT }

    [SerializeField] private nodeType own_State = nodeType.UNKNOWN;
    [Header("")]
    [SerializeField] private nodeType neighbour_State_N;
    [SerializeField] private nodeType neighbour_State_E;
    [SerializeField] private nodeType neighbour_State_S;
    [SerializeField] private nodeType neighbour_State_W;

    // Weltposition des Rasterelementes
    [Header("")]
    [SerializeField] private Vector2 worldPosition;

    public bool trash = false;

    public Vector2 WorldPosition { get => worldPosition; set => worldPosition = value; }
    public nodeType Neighbour_State_N { get => neighbour_State_N; set => neighbour_State_N = value; }
    public nodeType Neighbour_State_E { get => neighbour_State_E; set => neighbour_State_E = value; }
    public nodeType Neighbour_State_S { get => neighbour_State_S; set => neighbour_State_S = value; }
    public nodeType Neighbour_State_W { get => neighbour_State_W; set => neighbour_State_W = value; }
    public nodeType Own_State { get => own_State; set => own_State = value; }
    public bool Trash { get => trash; set => trash = value; }
}
