using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private float turnSpeed;

    private Rigidbody robotRigidbody;

    private void Start()
    {
        robotRigidbody = this.GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            this.transform.Translate(Vector3.forward * moveSpeed);
        }
        else
        {
            robotRigidbody.velocity = new Vector3(0, 0, 0);
        }

        if (Input.GetKey(KeyCode.E))
        {
            this.transform.Rotate(Vector3.up, turnSpeed);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            this.transform.Rotate(Vector3.down, turnSpeed);
        }
    }
}
