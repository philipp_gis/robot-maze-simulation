using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wallpoint : MonoBehaviour
{
    // Weltposition des Rasterelementes
    [SerializeField] private Vector2 worldPosition;

    public enum wall_Face { TBD, NORTH, EAST, SOUTH, WEST }

    // Weltposition des Rasterelementes
    [SerializeField] private wall_Face face;

    // Referenz auf letzt erstellten Wegpunkt
    [SerializeField] private GameObject last_Waypoint;

    public Vector2 WorldPosition { get => worldPosition; set => worldPosition = value; }
    public GameObject Last_Waypoint { get => last_Waypoint; set => last_Waypoint = value; }
    public wall_Face Face { get => face; set => face = value; }
}
