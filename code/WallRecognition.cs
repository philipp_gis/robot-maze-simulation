
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class WallRecognition : MonoBehaviour
{
    // Scant und speichert sich Wände ab

    #region Header
    private Data data;

    private Vector3 robotPosition;               // Weltposition Roboter
    private Quaternion robotRotation;            // Rotation Roboter

    // Richtungen der Sensoren
    Vector3 ne_Direction;
    Vector3 n_Direction;
    Vector3 nw_Direction;
    Vector3 w_Direction;
    Vector3 sw_Direction;
    Vector3 s_Direction;
    Vector3 se_Direction;
    Vector3 e_Direction;

    // Distanzen der gemeßenen Punkte
    float ne_Distance;
    float n_Distance;
    float nw_Distance;
    float w_Distance;
    float sw_Distance;
    float s_Distance;
    float se_Distance;
    float e_Distance;

    private void Awake()
    {
        data = GetComponent<Data>();

        ne_Direction = new Vector3(1, 0, 1);
        n_Direction = new Vector3(0, 0, 1);
        nw_Direction = new Vector3(-1, 0, 1);
        w_Direction = new Vector3(-1, 0, 0);
        sw_Direction = new Vector3(-1, 0, -1);
        s_Direction = new Vector3(0, 0, -1);
        se_Direction = new Vector3(1, 0, -1);
        e_Direction = new Vector3(1, 0, 0);
    }

    private void FixedUpdate()
    {
        // Rotation und Position soll jeden Frame abgefragt werden
        robotPosition = this.transform.localPosition;
        robotRotation = this.transform.localRotation;

        // Gemeßener Punkt soll jeden Frame abgefragt werden
        ne_Distance = this.GetComponent<Sensors>().NE_Distance;
        n_Distance = this.GetComponent<Sensors>().N_Distance;
        nw_Distance = this.GetComponent<Sensors>().NW_Distance;
        w_Distance = this.GetComponent<Sensors>().W_Distance;
        sw_Distance = this.GetComponent<Sensors>().SW_Distance;
        s_Distance = this.GetComponent<Sensors>().S_Distance;
        se_Distance = this.GetComponent<Sensors>().SE_Distance;
        e_Distance = this.GetComponent<Sensors>().E_Distance;

        wallRecognition(ne_Distance, ne_Direction, true);
        wallRecognition(n_Distance, n_Direction, false);
        wallRecognition(nw_Distance, nw_Direction, true);
        wallRecognition(w_Distance, w_Direction, false);
        wallRecognition(sw_Distance, sw_Direction, true);
        wallRecognition(s_Distance, s_Direction, false);
        wallRecognition(se_Distance, se_Direction, true);
        wallRecognition(e_Distance, e_Direction, false);
    }
    #endregion

    #region Methoden
    void wallRecognition(float which_sensor_Distance, Vector3 sensor_direction, bool is_diagonal)
    {
        if (which_sensor_Distance < 5.0f)
        {
            #region Berechne Wandpunkt
            Vector3 scanPoint_V3;
            if (is_diagonal)
            {
                // Berechne Weltposition vom abgetasteten Punkt
                float diagonalLength_CorrectionFaktor = 0.708333f;
                scanPoint_V3 = robotPosition + (robotRotation * sensor_direction * which_sensor_Distance * diagonalLength_CorrectionFaktor);
            }
            else
            {
                // Wenn Sensor diagonal abtastet, muss ein Korrekturfaktor drauf
                scanPoint_V3 = robotPosition + (robotRotation * sensor_direction * which_sensor_Distance);
            }

            // Wandel Weltposition in Vector2 um
            Vector2 scanPoint_V2 = new Vector2(scanPoint_V3.x, scanPoint_V3.z);

            Vector2 wallPoint;
            float i;
            // Schau ob der abgetastete Punkt links oder rechts vom Nullpunkt ist
            if (scanPoint_V2.x < 0)
            {
                i = 0;
                while (true)
                {
                    // Rechne solange ein Rasterelement hinzu bis das Rasterelement erreicht wurde 
                    if (i <= scanPoint_V2.x)
                    {
                        // Setze den Mittelpunkt des Rasterelementes in der X Achse
                        wallPoint.x = i + data.NodeRadius;
                        break;
                    }

                    i -= data.NodeDiameter;
                }
            }
            else
            {
                i = 0;
                while (true)
                {
                    if (i >= scanPoint_V2.x)
                    {
                        wallPoint.x = i - data.NodeRadius;
                        break;
                    }

                    i += data.NodeDiameter;
                }
            }

            // Schau ob der abgetastete Punkt oben oder unten vom Nullpunkt ist
            if (scanPoint_V2.y < 0)
            {
                i = 0;
                while (true)
                {
                    if (i <= scanPoint_V2.y)
                    {
                        wallPoint.y = i + data.NodeRadius;
                        break;
                    }

                    i -= data.NodeDiameter;
                }
            }
            else
            {
                i = 0;
                while (true)
                {
                    if (i >= scanPoint_V2.y)
                    {
                        wallPoint.y = i - data.NodeRadius;
                        break;
                    }

                    i += data.NodeDiameter;
                }
            }
            #endregion

            #region Duplikatsprüfung
            bool new_Wallpoint = true;
            // Prüfe ob Rasterelement schon mal gesetzt wurde
            if (data.WallpointList != null)
            {
                foreach (GameObject n in data.WallpointList)
                {
                    if (wallPoint == n.GetComponent<Wallpoint>().WorldPosition)
                    {
                        new_Wallpoint = false;
                    }
                }
            }
            #endregion

            #region Wegpunkt erstellen
            // Erstelle neues Rasterelement
            if (new_Wallpoint == true)
            {
                GameObject wallpoint = GameObject.CreatePrimitive(PrimitiveType.Cube);
                wallpoint.AddComponent<Wallpoint>();
                wallpoint.GetComponent<Wallpoint>().WorldPosition = wallPoint;
                wallpoint.transform.localScale = data.NodeSize;
                wallpoint.transform.position = new Vector3(wallPoint.x, -0.50f, wallPoint.y);
                wallpoint.GetComponent<Renderer>().material.color = Color.red;
                wallpoint.transform.parent = data.WallpointFolder.transform;
                data.WallpointList.Add(wallpoint);

                // Berechnung ist auf den Nordsensor ausgelegt
                // direction Faktor rechnet auf die anderen Sensorrichtungen um
                float d_Factor = 0f;

                if (sensor_direction == n_Direction) { d_Factor = 0f; }
                if (sensor_direction == ne_Direction) { d_Factor = 45f; }
                if (sensor_direction == e_Direction) { d_Factor = 90f; }
                if (sensor_direction == se_Direction) { d_Factor = 135f; }
                if (sensor_direction == s_Direction) { d_Factor = 180f; }
                if (sensor_direction == sw_Direction) { d_Factor = 225f; }
                if (sensor_direction == w_Direction) { d_Factor = 270f; }
                if (sensor_direction == nw_Direction) { d_Factor = 315f; }

                // Modulo passt auf das wir unter 360° bleiben
                float d_Value = (robotRotation.eulerAngles.y + d_Factor) % 360;

                // Wenn wir vom Winkel her nach Norden schauen ist auch das Wandelement nach Norden ausgerichtet
                if (d_Value >= 0f && d_Value < 45f)
                {
                    wallpoint.GetComponent<Wallpoint>().Face = Wallpoint.wall_Face.NORTH;
                }

                if (d_Value >= 315f && d_Value < 360f)
                {
                    wallpoint.GetComponent<Wallpoint>().Face = Wallpoint.wall_Face.NORTH;
                }

                if (d_Value >= 225f && d_Value < 315f)
                {
                    wallpoint.GetComponent<Wallpoint>().Face = Wallpoint.wall_Face.WEST;
                }

                if (d_Value >= 135f && d_Value < 225f)
                {
                    wallpoint.GetComponent<Wallpoint>().Face = Wallpoint.wall_Face.SOUTH;
                }

                if (d_Value >= 45f && d_Value < 135f)
                {
                    wallpoint.GetComponent<Wallpoint>().Face = Wallpoint.wall_Face.EAST;
                }
            }
            #endregion
        }
    }
    #endregion
}
