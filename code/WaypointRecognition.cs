using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointRecognition : MonoBehaviour
{
    private Data data;

    // Referenz des erzeugenden Rasterelements
    GameObject node;

    private int old_Count_WallpointList;
    private int new_Count_WallpointList;
    private int old_Count_WaypointList;
    private int new_Count_WaypointList;

    private void Awake()
    {
        data = GetComponent<Data>();

        old_Count_WallpointList = 0;
        new_Count_WallpointList = 0;
        old_Count_WaypointList = 0;
        new_Count_WaypointList = 0;
    }

    private void FixedUpdate()
    {
        ConerDedection();
        Waypoint_Cleaner();
        DrawTransition();
    }

    public void ConerDedection()
    {
        // Erkennt innere und äußere Ecken

        new_Count_WallpointList = data.WallpointList.Count;

        // Wird nur aufgerufen wenn sich etwas in der Wandpunktliste ändert
        if (old_Count_WallpointList != new_Count_WallpointList)
        {
            #region Setup
            bool be = false;
            bool bs = false;
            bool bw = false;
            bool bn = false;

            bool bep = false;
            bool bsp = false;
            bool bwp = false;
            bool bnp = false;

            bool bepp = false;
            bool bspp = false;
            bool bwpp = false;
            bool bnpp = false;

            bool beppp = false;
            bool bsppp = false;
            bool bwppp = false;
            bool bnppp = false;

            bool bnw = false;
            bool bne = false;
            bool bsw = false;
            bool bse = false;

            bool bnwp = false;
            bool bnep = false;
            bool bswp = false;
            bool bsep = false;

            bool bnnep = false;
            bool bneep = false;
            bool bseep = false;
            bool bssep = false;
            bool bsswp = false;
            bool bswwp = false;
            bool bnwwp = false;
            bool bnnwp = false;

            Vector2 east;
            Vector2 south;
            Vector2 west;
            Vector2 north;

            Vector2 ep;
            Vector2 sp;
            Vector2 wp;
            Vector2 np;

            Vector2 epp;
            Vector2 spp;
            Vector2 wpp;
            Vector2 npp;

            Vector2 eppp;
            Vector2 sppp;
            Vector2 wppp;
            Vector2 nppp;

            Vector2 ne;
            Vector2 se;
            Vector2 sw;
            Vector2 nw;

            Vector2 nep;
            Vector2 sep;
            Vector2 swp;
            Vector2 nwp;

            Vector2 nnep;
            Vector2 neep;
            Vector2 seep;
            Vector2 ssep;
            Vector2 sswp;
            Vector2 swwp;
            Vector2 nwwp;
            Vector2 nnwp;

            #endregion

            foreach (GameObject n in data.WallpointList)
            {
                #region Mustererkennung
                node = n;
                float n_x = n.GetComponent<Wallpoint>().WorldPosition.x;
                float n_y = n.GetComponent<Wallpoint>().WorldPosition.y;

                north = new Vector2(n_x, n_y + data.NodeDiameter);
                south = new Vector2(n_x, n_y - data.NodeDiameter);
                east = new Vector2(n_x + data.NodeDiameter, n_y);
                west = new Vector2(n_x - data.NodeDiameter, n_y);

                np = new Vector2(n_x, n_y + (data.NodeDiameter * 2));
                sp = new Vector2(n_x, n_y - (data.NodeDiameter * 2));
                ep = new Vector2(n_x + (data.NodeDiameter * 2), n_y);
                wp = new Vector2(n_x - (data.NodeDiameter * 2), n_y);

                npp = new Vector2(n_x, n_y + (data.NodeDiameter * 3));
                spp = new Vector2(n_x, n_y - (data.NodeDiameter * 3));
                epp = new Vector2(n_x + (data.NodeDiameter * 3), n_y);
                wpp = new Vector2(n_x - (data.NodeDiameter * 3), n_y);

                nppp = new Vector2(n_x, n_y + (data.NodeDiameter * 4));
                sppp = new Vector2(n_x, n_y - (data.NodeDiameter * 4));
                eppp = new Vector2(n_x + (data.NodeDiameter * 4), n_y);
                wppp = new Vector2(n_x - (data.NodeDiameter * 4), n_y);

                ne = new Vector2(n_x + data.NodeDiameter, n_y + data.NodeDiameter);
                sw = new Vector2(n_x - data.NodeDiameter, n_y - data.NodeDiameter);
                se = new Vector2(n_x + data.NodeDiameter, n_y - data.NodeDiameter);
                nw = new Vector2(n_x - data.NodeDiameter, n_y + data.NodeDiameter);

                nep = new Vector2(n_x + (data.NodeDiameter * 2), n_y + (data.NodeDiameter * 2));
                swp = new Vector2(n_x - (data.NodeDiameter * 2), n_y - (data.NodeDiameter * 2));
                sep = new Vector2(n_x + (data.NodeDiameter * 2), n_y - (data.NodeDiameter * 2));
                nwp = new Vector2(n_x - (data.NodeDiameter * 2), n_y + (data.NodeDiameter * 2));
                nnep = new Vector2(n_x + (data.NodeDiameter * 1), n_y + (data.NodeDiameter * 2));
                neep = new Vector2(n_x + (data.NodeDiameter * 2), n_y + (data.NodeDiameter * 1));
                seep = new Vector2(n_x + (data.NodeDiameter * 2), n_y - (data.NodeDiameter * 1));
                ssep = new Vector2(n_x + (data.NodeDiameter * 1), n_y - (data.NodeDiameter * 2));
                sswp = new Vector2(n_x - (data.NodeDiameter * 1), n_y - (data.NodeDiameter * 2));
                swwp = new Vector2(n_x - (data.NodeDiameter * 2), n_y - (data.NodeDiameter * 1));
                nwwp = new Vector2(n_x - (data.NodeDiameter * 2), n_y + (data.NodeDiameter * 1));
                nnwp = new Vector2(n_x - (data.NodeDiameter * 1), n_y + (data.NodeDiameter * 2));

                // Vergleicht die Nachbarpositionen des Rasterelement und merkt sich die Nachbarn die dieses Rasterelement besitzt
                foreach (GameObject r in data.WallpointList)
                {
                    Vector2 n_pos = r.GetComponent<Wallpoint>().WorldPosition;

                    if (n_pos == east) { be = true; }
                    if (n_pos == west) { bw = true; }
                    if (n_pos == north) { bn = true; }
                    if (n_pos == south) { bs = true; }

                    if (n_pos == ep) { bep = true; }
                    if (n_pos == wp) { bwp = true; }
                    if (n_pos == np) { bnp = true; }
                    if (n_pos == sp) { bsp = true; }

                    if (n_pos == epp) { bepp = true; }
                    if (n_pos == wpp) { bwpp = true; }
                    if (n_pos == npp) { bnpp = true; }
                    if (n_pos == spp) { bspp = true; }

                    if (n_pos == eppp) { beppp = true; }
                    if (n_pos == wppp) { bwppp = true; }
                    if (n_pos == nppp) { bnppp = true; }
                    if (n_pos == sppp) { bsppp = true; }

                    if (n_pos == nw) { bnw = true; }
                    if (n_pos == ne) { bne = true; }
                    if (n_pos == sw) { bsw = true; }
                    if (n_pos == se) { bse = true; }

                    if (n_pos == nwp) { bnwp = true; }
                    if (n_pos == nep) { bnep = true; }
                    if (n_pos == swp) { bswp = true; }
                    if (n_pos == sep) { bsep = true; }

                    if (n_pos == nnep) { bnnep = true; }
                    if (n_pos == neep) { bneep = true; }
                    if (n_pos == seep) { bseep = true; }
                    if (n_pos == ssep) { bssep = true; }
                    if (n_pos == sswp) { bsswp = true; }
                    if (n_pos == swwp) { bswwp = true; }
                    if (n_pos == nwwp) { bnwwp = true; }
                    if (n_pos == nnwp) { bnnwp = true; }
                }

                #endregion

                #region Musterabgleich
                // Raum Ausgang
                if (bn && bnp && !bne && !be && !bep && !bse && !bs && bsp && bspp && bsppp)
                {
                    n.GetComponent<Renderer>().material.color = Color.green;
                    createWaypoint(se, n, Waypoint.type.TE);
                }

                else if (bn && bnp && !bnw && !bw && !bwp && !bsw && !bs && bsp && bspp && bsppp)
                {
                    n.GetComponent<Renderer>().material.color = Color.green;
                    createWaypoint(sw, n, Waypoint.type.TW);
                }

                else if (bw && bwp && !bse && !bs && !bsp && !bsw && !be && bep && bepp && beppp)
                {
                    n.GetComponent<Renderer>().material.color = Color.green;
                    createWaypoint(se, n, Waypoint.type.TS);
                }

                else if (bw && bwp && !bne && !bn && !bnp && !bnw && !be && bep && bepp && beppp)
                {
                    n.GetComponent<Renderer>().material.color = Color.green;
                    createWaypoint(ne, n, Waypoint.type.TN);
                }

                // Kreuzung
                else if (!be && bep && !bs && bsp && bsep && !bseep && !bssep)
                {
                    n.GetComponent<Renderer>().material.color = Color.cyan;
                    createWaypoint(se, n, Waypoint.type.X);
                }

                // Durchgang
                else if (bn && bnp && !bne && bnep && !be && !bep && !bse && bsep && bs && bsp)
                {
                    n.GetComponent<Renderer>().material.color = Color.green;
                    createWaypoint(east, n, Waypoint.type.TW);
                }

                else if (bn && bnp && !bnw && bnwp && !bw && !bwp && !bsw && bswp && bs && bsp)
                {
                    n.GetComponent<Renderer>().material.color = Color.green;
                    createWaypoint(west, n, Waypoint.type.TE);
                }

                else if (be && bep && !bne && bnep && !bn && !bnp && !bnw && bnwp && bw && bwp)
                {
                    n.GetComponent<Renderer>().material.color = Color.green;
                    createWaypoint(north, n, Waypoint.type.TS);
                }

                else if (be && bep && !bse && bsep && !bs && !bsp && !bsw && bswp && bw && bwp)
                {
                    n.GetComponent<Renderer>().material.color = Color.green;
                    createWaypoint(south, n, Waypoint.type.TN);
                }

                // Sackgasse
                else if (be && bw && !bs && bse && bsw)
                {
                    n.GetComponent<Renderer>().material.color = Color.green;
                    createWaypoint(south, n, Waypoint.type.IS);
                }

                else if (be && bw && !bn && bne && bnw)
                {
                    n.GetComponent<Renderer>().material.color = Color.green;
                    createWaypoint(north, n, Waypoint.type.IN);
                }

                else if (bn && bs && !bw && bsw && bnw)
                {
                    n.GetComponent<Renderer>().material.color = Color.green;
                    createWaypoint(west, n, Waypoint.type.IW);
                }

                else if (bn && bs && !be && bse && bne)
                {
                    n.GetComponent<Renderer>().material.color = Color.green;
                    createWaypoint(east, n, Waypoint.type.IE);
                }

                // Ecken Innen Erkennung
                else if (bn && be && !bne && !bnnep && !bneep)
                {
                    n.GetComponent<Renderer>().material.color = Color.green;
                    createWaypoint(ne, n, Waypoint.type.LNE);
                }

                else if (bs && be && !bse && !bseep && !bssep)
                {
                    n.GetComponent<Renderer>().material.color = Color.green;
                    createWaypoint(se, n, Waypoint.type.LSE);
                }

                else if (bs && bw && !bsw && !bsswp && !bswwp)
                {
                    n.GetComponent<Renderer>().material.color = Color.green;
                    createWaypoint(sw, n, Waypoint.type.LSW);
                }

                else if (bn && bw && !bnw && !bnwwp && !bnnwp)
                {
                    n.GetComponent<Renderer>().material.color = Color.green;
                    createWaypoint(nw, n, Waypoint.type.LNW);
                }

                else
                {
                    n.GetComponent<Renderer>().material.color = Color.red;
                    n.GetComponent<Wallpoint>().Last_Waypoint = null;
                }

                bn = false;
                be = false;
                bs = false;
                bw = false;

                bnp = false;
                bep = false;
                bsp = false;
                bwp = false;

                bnpp = false;
                bepp = false;
                bspp = false;
                bwpp = false;

                bnppp = false;
                beppp = false;
                bsppp = false;
                bwppp = false;

                bnw = false;
                bne = false;
                bsw = false;
                bse = false;

                bnwp = false;
                bnep = false;
                bswp = false;
                bsep = false;

                bnnep = false;
                bneep = false;
                bseep = false;
                bssep = false;
                bsswp = false;
                bswwp = false;
                bnwwp = false;
                bnnwp = false;

                #endregion
            }
        }
        old_Count_WallpointList = new_Count_WallpointList;
    }

    void createWaypoint(Vector2 worldposition, GameObject creator_node, Waypoint.type w_type)
    {
        // Erstellt einen neuen Wegpunkt

        bool new_waypoint = true;

        #region Duplikatsprüfung
        if (data.WaypointList.Count != 0)
        {
            foreach (GameObject w in data.WaypointList)
            {
                if (worldposition == w.GetComponent<Waypoint>().WorldPosition)
                {
                    new_waypoint = false;
                }
            }
        }

        if (data.WallpointList.Count != 0)
        {
            foreach (GameObject w in data.WaypointList)
            {
                if (worldposition == w.GetComponent<Waypoint>().WorldPosition)
                {
                    new_waypoint = false;
                }
            }
        }
        #endregion

        #region Wegpunkt Erstellung
        if (new_waypoint)
        {
            GameObject wayPoint = GameObject.CreatePrimitive(PrimitiveType.Cylinder);

            wayPoint.AddComponent<Waypoint>();
            wayPoint.GetComponent<Waypoint>().WorldPosition = worldposition;
            wayPoint.GetComponent<Waypoint>().Trash = true;
            wayPoint.GetComponent<Waypoint>().Waypoint_Type = w_type;
            wayPoint.GetComponent<Renderer>().material.color = Color.yellow;

            if (wayPoint.GetComponent<Waypoint>().Waypoint_Type == Waypoint.type.X)
            {
                wayPoint.GetComponent<Waypoint>().State_N = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_E = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_S = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_W = Waypoint.state.WAY;
            }

            else if (wayPoint.GetComponent<Waypoint>().Waypoint_Type == Waypoint.type.TN)
            {
                wayPoint.GetComponent<Waypoint>().State_N = Waypoint.state.WALL;
                wayPoint.GetComponent<Waypoint>().State_E = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_S = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_W = Waypoint.state.WAY;

                wayPoint.GetComponent<Waypoint>().Reference_N = null;
            }

            else if (wayPoint.GetComponent<Waypoint>().Waypoint_Type == Waypoint.type.TE)
            {
                wayPoint.GetComponent<Waypoint>().State_N = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_E = Waypoint.state.WALL;
                wayPoint.GetComponent<Waypoint>().State_S = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_W = Waypoint.state.WAY;

                wayPoint.GetComponent<Waypoint>().Reference_E = null;
            }

            else if (wayPoint.GetComponent<Waypoint>().Waypoint_Type == Waypoint.type.TS)
            {
                wayPoint.GetComponent<Waypoint>().State_N = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_E = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_S = Waypoint.state.WALL;
                wayPoint.GetComponent<Waypoint>().State_W = Waypoint.state.WAY;

                wayPoint.GetComponent<Waypoint>().Reference_S = null;
            }

            else if (wayPoint.GetComponent<Waypoint>().Waypoint_Type == Waypoint.type.TW)
            {
                wayPoint.GetComponent<Waypoint>().State_N = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_E = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_S = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_W = Waypoint.state.WALL;

                wayPoint.GetComponent<Waypoint>().Reference_W = null;
            }

            else if (wayPoint.GetComponent<Waypoint>().Waypoint_Type == Waypoint.type.LNW)
            {
                wayPoint.GetComponent<Waypoint>().State_N = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_E = Waypoint.state.WALL;
                wayPoint.GetComponent<Waypoint>().State_S = Waypoint.state.WALL;
                wayPoint.GetComponent<Waypoint>().State_W = Waypoint.state.WAY;

                wayPoint.GetComponent<Waypoint>().Reference_E = null;
                wayPoint.GetComponent<Waypoint>().Reference_S = null;
            }

            else if (wayPoint.GetComponent<Waypoint>().Waypoint_Type == Waypoint.type.LNE)
            {
                wayPoint.GetComponent<Waypoint>().State_N = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_E = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_S = Waypoint.state.WALL;
                wayPoint.GetComponent<Waypoint>().State_W = Waypoint.state.WALL;

                wayPoint.GetComponent<Waypoint>().Reference_S = null;
                wayPoint.GetComponent<Waypoint>().Reference_W = null;
            }

            else if (wayPoint.GetComponent<Waypoint>().Waypoint_Type == Waypoint.type.LSE)
            {
                wayPoint.GetComponent<Waypoint>().State_N = Waypoint.state.WALL;
                wayPoint.GetComponent<Waypoint>().State_E = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_S = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_W = Waypoint.state.WALL;

                wayPoint.GetComponent<Waypoint>().Reference_N = null;
                wayPoint.GetComponent<Waypoint>().Reference_W = null;
            }

            else if (wayPoint.GetComponent<Waypoint>().Waypoint_Type == Waypoint.type.LSW)
            {
                wayPoint.GetComponent<Waypoint>().State_N = Waypoint.state.WALL;
                wayPoint.GetComponent<Waypoint>().State_E = Waypoint.state.WALL;
                wayPoint.GetComponent<Waypoint>().State_S = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_W = Waypoint.state.WAY;

                wayPoint.GetComponent<Waypoint>().Reference_N = null;
                wayPoint.GetComponent<Waypoint>().Reference_E = null;
            }

            else if (wayPoint.GetComponent<Waypoint>().Waypoint_Type == Waypoint.type.IN)
            {
                wayPoint.GetComponent<Waypoint>().State_N = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_E = Waypoint.state.WALL;
                wayPoint.GetComponent<Waypoint>().State_S = Waypoint.state.WALL;
                wayPoint.GetComponent<Waypoint>().State_W = Waypoint.state.WALL;

                wayPoint.GetComponent<Waypoint>().Reference_E = null;
                wayPoint.GetComponent<Waypoint>().Reference_S = null;
                wayPoint.GetComponent<Waypoint>().Reference_W = null;
            }

            else if (wayPoint.GetComponent<Waypoint>().Waypoint_Type == Waypoint.type.IE)
            {
                wayPoint.GetComponent<Waypoint>().State_N = Waypoint.state.WALL;
                wayPoint.GetComponent<Waypoint>().State_E = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_S = Waypoint.state.WALL;
                wayPoint.GetComponent<Waypoint>().State_W = Waypoint.state.WALL;

                wayPoint.GetComponent<Waypoint>().Reference_N = null;
                wayPoint.GetComponent<Waypoint>().Reference_S = null;
                wayPoint.GetComponent<Waypoint>().Reference_W = null;
            }

            else if (wayPoint.GetComponent<Waypoint>().Waypoint_Type == Waypoint.type.IS)
            {
                wayPoint.GetComponent<Waypoint>().State_N = Waypoint.state.WALL;
                wayPoint.GetComponent<Waypoint>().State_E = Waypoint.state.WALL;
                wayPoint.GetComponent<Waypoint>().State_S = Waypoint.state.WAY;
                wayPoint.GetComponent<Waypoint>().State_W = Waypoint.state.WALL;

                wayPoint.GetComponent<Waypoint>().Reference_N = null;
                wayPoint.GetComponent<Waypoint>().Reference_E = null;
                wayPoint.GetComponent<Waypoint>().Reference_W = null;
            }

            else if (wayPoint.GetComponent<Waypoint>().Waypoint_Type == Waypoint.type.IW)
            {
                wayPoint.GetComponent<Waypoint>().State_N = Waypoint.state.WALL;
                wayPoint.GetComponent<Waypoint>().State_E = Waypoint.state.WALL;
                wayPoint.GetComponent<Waypoint>().State_S = Waypoint.state.WALL;
                wayPoint.GetComponent<Waypoint>().State_W = Waypoint.state.WAY;

                wayPoint.GetComponent<Waypoint>().Reference_N = null;
                wayPoint.GetComponent<Waypoint>().Reference_E = null;
                wayPoint.GetComponent<Waypoint>().Reference_S = null;
            }

            node.GetComponent<Wallpoint>().Last_Waypoint = wayPoint;

            wayPoint.transform.localScale = data.NodeSize * 0.5f;
            wayPoint.transform.position = new Vector3(worldposition.x, -0.50f, worldposition.y);
            wayPoint.transform.parent = data.WaypointFolder.transform;

            Check_Transition(wayPoint, w_type);

            data.WaypointList.Add(wayPoint);

        }
        #endregion

        new_waypoint = true;
    }

    public void Waypoint_Cleaner()
    {
        new_Count_WaypointList = data.WaypointList.Count;

        // Wird nur aufgerufen wenn sich etwas in der Wegpunktliste ändert
        if (old_Count_WaypointList != new_Count_WaypointList)
        {
            #region Markierung veralteter Wegpunkte 
            if (data.WallpointList.Count != 0)
            {
                foreach (GameObject n in data.WallpointList)
                {
                    if (data.WaypointList.Count != 0)
                    {
                        foreach (GameObject w in data.WaypointList)
                        {
                            if (n.GetComponent<Wallpoint>().Last_Waypoint == w)
                            {
                                w.GetComponent<Waypoint>().Trash = false;
                            }

                            if (n.GetComponent<Wallpoint>().WorldPosition == w.GetComponent<Waypoint>().WorldPosition)
                            {
                                w.GetComponent<Waypoint>().Trash = true;
                            }
                        }
                    }
                }
            }
            #endregion

            #region Löschung markierter Wegpunkte
            for (int i = 0; i < data.WaypointList.Count; i++)
            {
                if (data.WaypointList[i].GetComponent<Waypoint>().Trash == true)
                {
                    GameObject toDestroy = data.WaypointList[i];
                    GameObject n_w = null;
                    GameObject e_w = null;
                    GameObject s_w = null;
                    GameObject w_w = null;

                    bool n_to_s = false;
                    bool e_to_w = false;

                    if (toDestroy.GetComponent<Waypoint>().Reference_N && toDestroy.GetComponent<Waypoint>().Reference_S)
                    {
                        n_w = toDestroy.GetComponent<Waypoint>().Reference_N;
                        s_w = toDestroy.GetComponent<Waypoint>().Reference_S;

                        n_to_s = true;
                    }

                    if (toDestroy.GetComponent<Waypoint>().Reference_E && toDestroy.GetComponent<Waypoint>().Reference_W)
                    {
                        e_w = toDestroy.GetComponent<Waypoint>().Reference_E;
                        w_w = toDestroy.GetComponent<Waypoint>().Reference_W;

                        e_to_w = true;
                    }

                    if (toDestroy.GetComponent<Waypoint>().Reference_N)
                    {
                        toDestroy.GetComponent<Waypoint>().Reference_N.GetComponent<Waypoint>().Reference_S = null;
                    }

                    if (toDestroy.GetComponent<Waypoint>().Reference_E)
                    {
                        toDestroy.GetComponent<Waypoint>().Reference_E.GetComponent<Waypoint>().Reference_W = null;
                    }

                    if (toDestroy.GetComponent<Waypoint>().Reference_S)
                    {
                        toDestroy.GetComponent<Waypoint>().Reference_S.GetComponent<Waypoint>().Reference_N = null;
                    }

                    if (toDestroy.GetComponent<Waypoint>().Reference_W)
                    {
                        toDestroy.GetComponent<Waypoint>().Reference_W.GetComponent<Waypoint>().Reference_E = null;
                    }

                    data.WaypointList.Remove(data.WaypointList[i]);
                    Destroy(toDestroy);

                    if (n_to_s)
                    {
                        n_w.GetComponent<Waypoint>().Reference_S = s_w;
                        s_w.GetComponent<Waypoint>().Reference_N = n_w;
                    }

                    if (e_to_w)
                    {
                        Debug.Log(e_w.GetComponent<Waypoint>().WorldPosition);
                        Debug.Log(w_w.GetComponent<Waypoint>().WorldPosition);
                        e_w.GetComponent<Waypoint>().Reference_W = w_w;
                        w_w.GetComponent<Waypoint>().Reference_E = e_w;
                        Debug.Log("blup");
                    }
                }
            }

            foreach (GameObject w in data.WaypointList)
            {
                w.GetComponent<Waypoint>().Trash = true;
            }
            #endregion
        }
        old_Count_WaypointList = new_Count_WaypointList;
    }

    void Check_Transition(GameObject this_w, Waypoint.type w_type)
    {

        if (w_type == Waypoint.type.IN)
        {
            North_Check(this_w);
        }

        else if (w_type == Waypoint.type.IE)
        {
            East_Check(this_w);
        }

        else if (w_type == Waypoint.type.IS)
        {
            South_Check(this_w);
        }

        else if (w_type == Waypoint.type.IW)
        {
            West_Check(this_w);
        }

        else if (w_type == Waypoint.type.LNE)
        {
            North_Check(this_w);
            East_Check(this_w);
        }

        else if (w_type == Waypoint.type.LSE)
        {
            South_Check(this_w);
            East_Check(this_w);
        }

        else if (w_type == Waypoint.type.LSW)
        {
            South_Check(this_w);
            West_Check(this_w);
        }

        else if (w_type == Waypoint.type.LNW)
        {
            North_Check(this_w);
            West_Check(this_w);
        }

        else if (w_type == Waypoint.type.TN)
        {
            East_Check(this_w);
            South_Check(this_w);
            West_Check(this_w);
        }

        else if (w_type == Waypoint.type.TE)
        {
            North_Check(this_w);
            South_Check(this_w);
            West_Check(this_w);
        }

        else if (w_type == Waypoint.type.TS)
        {
            North_Check(this_w);
            East_Check(this_w);
            West_Check(this_w);
        }

        else if (w_type == Waypoint.type.TW)
        {
            North_Check(this_w);
            East_Check(this_w);
            South_Check(this_w);
        }

        else if (w_type == Waypoint.type.X)
        {
            North_Check(this_w);
            East_Check(this_w);
            South_Check(this_w);
            West_Check(this_w);
        }
    }

    void North_Check(GameObject this_w)
    {
        if (this_w.GetComponent<Waypoint>().Reference_N == null)
        {
            GameObject nearest_w = null;
            float nearest_y = 0f;

            foreach (GameObject other_w in data.WaypointList)
            {
                if (this_w != other_w)
                {
                    float this_x = this_w.GetComponent<Waypoint>().WorldPosition.x;
                    float this_y = this_w.GetComponent<Waypoint>().WorldPosition.y;
                    float other_x = other_w.GetComponent<Waypoint>().WorldPosition.x;
                    float other_y = other_w.GetComponent<Waypoint>().WorldPosition.y;

                    // Gleiche X Achse wie prüfender Punkt
                    if (other_x >= this_x - 0.1f && other_x <= this_x + 0.1f)
                    {
                        // Südlich vom prüfenden Punkt
                        if (other_y > this_y)
                        {
                            if (nearest_w == null)
                            {
                                nearest_w = other_w;
                                nearest_y = nearest_w.GetComponent<Waypoint>().WorldPosition.y;
                            }
                            else
                            {
                                if (other_y < nearest_y)
                                {
                                    nearest_w = other_w;
                                }
                            }
                        }
                    }
                }
            }

            if (nearest_w != null)
            {
                if (this_w.GetComponent<Waypoint>().State_N != Waypoint.state.WALL && nearest_w.GetComponent<Waypoint>().State_S != Waypoint.state.WALL)
                {
                    this_w.GetComponent<Waypoint>().Reference_N = nearest_w;
                    nearest_w.GetComponent<Waypoint>().Reference_S = this_w;
                }
            }
        }
    }

    void East_Check(GameObject this_w)
    {
        if (this_w.GetComponent<Waypoint>().Reference_E == null)
        {
            GameObject nearest_w = null;
            float nearest_x = 0f;

            foreach (GameObject other_w in data.WaypointList)
            {
                if (this_w != other_w)
                {
                    float this_x = this_w.GetComponent<Waypoint>().WorldPosition.x;
                    float this_y = this_w.GetComponent<Waypoint>().WorldPosition.y;
                    float other_x = other_w.GetComponent<Waypoint>().WorldPosition.x;
                    float other_y = other_w.GetComponent<Waypoint>().WorldPosition.y;

                    // Gleiche Y Achse wie prüfender Punkt
                    if (other_y >= this_y - 0.1f && other_y <= this_y + 0.1f)
                    {
                        // Östlich vom prüfenden Punkt
                        if (other_x > this_x)
                        {
                            if (nearest_w == null)
                            {
                                nearest_w = other_w;
                                nearest_x = nearest_w.GetComponent<Waypoint>().WorldPosition.x;
                            }
                            else
                            {
                                if (other_x < nearest_x)
                                {
                                    nearest_w = other_w;
                                }
                            }
                        }
                    }
                }
            }

            if (nearest_w != null)
            {
               if (this_w.GetComponent<Waypoint>().State_E != Waypoint.state.WALL && nearest_w.GetComponent<Waypoint>().State_W != Waypoint.state.WALL)
               {
                    this_w.GetComponent<Waypoint>().Reference_E = nearest_w;
                    nearest_w.GetComponent<Waypoint>().Reference_W = this_w;
               }
            }
        }
    }

    void South_Check(GameObject this_w)
    {
        if (this_w.GetComponent<Waypoint>().Reference_S == null)
        {
            GameObject nearest_w = null;
            float nearest_y = 0f;

            foreach (GameObject other_w in data.WaypointList)
            {
                if (this_w != other_w)
                {
                    float this_x = this_w.GetComponent<Waypoint>().WorldPosition.x;
                    float this_y = this_w.GetComponent<Waypoint>().WorldPosition.y;
                    float other_x = other_w.GetComponent<Waypoint>().WorldPosition.x;
                    float other_y = other_w.GetComponent<Waypoint>().WorldPosition.y;

                    // Gleiche X Achse wie prüfender Punkt
                    if (other_x >= this_x - 0.1f && other_x <= this_x + 0.1f)
                    {
                        // Südlich vom prüfenden Punkt
                        if (other_y < this_y)
                        {
                            if (nearest_w == null)
                            {
                                nearest_w = other_w;
                                nearest_y = nearest_w.GetComponent<Waypoint>().WorldPosition.y;
                            }
                            else
                            {
                                if (other_y > nearest_y)
                                {
                                    nearest_w = other_w;
                                }
                            }
                        }
                    }
                }
            }

            if (nearest_w != null)
            {
              if (this_w.GetComponent<Waypoint>().State_S != Waypoint.state.WALL && nearest_w.GetComponent<Waypoint>().State_N != Waypoint.state.WALL)
               {
                    this_w.GetComponent<Waypoint>().Reference_S = nearest_w;
                    nearest_w.GetComponent<Waypoint>().Reference_N = this_w;
               }
            }
        }
    }

    void West_Check(GameObject this_w)
    {
        if (this_w.GetComponent<Waypoint>().Reference_W == null)
        {
            GameObject nearest_w = null;
            float nearest_x = 0f;

            foreach (GameObject other_w in data.WaypointList)
            {
                if (this_w != other_w)
                {
                    float this_x = this_w.GetComponent<Waypoint>().WorldPosition.x;
                    float this_y = this_w.GetComponent<Waypoint>().WorldPosition.y;
                    float other_x = other_w.GetComponent<Waypoint>().WorldPosition.x;
                    float other_y = other_w.GetComponent<Waypoint>().WorldPosition.y;

                    // Gleiche Y Achse wie prüfender Punkt
                    if (other_y >= this_y - 0.1f && other_y <= this_y + 0.1f)
                    {
                        // westlich vom prüfenden Punkt
                        if (other_x < this_x)
                        {
                            if (nearest_w == null)
                            {
                                nearest_w = other_w;
                                nearest_x = nearest_w.GetComponent<Waypoint>().WorldPosition.x;
                            }
                            else
                            {
                                if (other_x > nearest_x)
                                {
                                    nearest_w = other_w;
                                }
                            }
                        }
                    }
                }
            }

            if (nearest_w != null)
            {
               if (this_w.GetComponent<Waypoint>().State_W != Waypoint.state.WALL && nearest_w.GetComponent<Waypoint>().State_E != Waypoint.state.WALL)
               {
                    this_w.GetComponent<Waypoint>().Reference_W = nearest_w;
                    nearest_w.GetComponent<Waypoint>().Reference_E = this_w;
               }
            }
        }
    }

    public void DrawTransition()
    {
        foreach (GameObject w in data.WaypointList)
        {
            if (w.GetComponent<Waypoint>().Reference_N != null)
            {
                Debug.DrawLine(w.transform.position, w.GetComponent<Waypoint>().Reference_N.transform.position, Color.yellow);
            }

            if (w.GetComponent<Waypoint>().Reference_E != null)
            {
                Debug.DrawLine(w.transform.position, w.GetComponent<Waypoint>().Reference_E.transform.position, Color.yellow);
            }

            if (w.GetComponent<Waypoint>().Reference_S != null)
            {
                Debug.DrawLine(w.transform.position, w.GetComponent<Waypoint>().Reference_S.transform.position, Color.yellow);
            }

            if (w.GetComponent<Waypoint>().Reference_W != null)
            {
                Debug.DrawLine(w.transform.position, w.GetComponent<Waypoint>().Reference_W.transform.position, Color.yellow);
            }
        }
    }
}
