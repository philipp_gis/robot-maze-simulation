using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data : MonoBehaviour
{
    [SerializeField] private GameObject wallpoint_Folder;       // Unity Ordner in den die Rasterelemente abgelegt werden
    [SerializeField] private GameObject waypoint_Folder;        // Unity Ordner in den die Wegpunkte abgelegt werden

    [Header("")]
    [SerializeField] private float nodeDiameter;                // Durchmesser eines Rasterelement
    [SerializeField] private float nodeGap;                     // Freier Platz zwischen der Rasterelemente

    [Header("")]
    [SerializeField] private List<GameObject> wallpointList;    // Liste mit allen Rasterelementen
    [SerializeField] private List<GameObject> waypointList;     // Liste mit allen Wegpunkte

    private Vector3 nodeSize;                                   // Transformation eines Rasterelement
    private float nodeRadius;                                   // Radius eines Rasterelement

    private void Awake()
    {
        // Rasterelemente sollen wie flache Scheiben wirken und Konturen haben
        nodeSize = new Vector3(nodeDiameter - nodeGap, 0.1f, nodeDiameter - nodeGap);
        nodeRadius = nodeDiameter * 0.5f;

        wallpointList = new List<GameObject> { };
        waypointList = new List<GameObject> { };
    }

    public List<GameObject> WallpointList { get => wallpointList; }
    public List<GameObject> WaypointList { get => waypointList; set => waypointList = value; }
    public float NodeDiameter { get => nodeDiameter; set => nodeDiameter = value; }
    public float NodeGap { get => nodeGap; set => nodeGap = value; }
    public Vector3 NodeSize { get => nodeSize; set => nodeSize = value; }
    public float NodeRadius { get => nodeRadius; set => nodeRadius = value; }
    public GameObject WallpointFolder { get => wallpoint_Folder; set => wallpoint_Folder = value; }
    public GameObject WaypointFolder { get => waypoint_Folder; set => waypoint_Folder = value; }
}
