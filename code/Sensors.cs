#region Header

// Simuliert die Sensoren des Roboters und gibt die gemessene Distanz wieder

using UnityEngine;
using UnityEngine.UI;

public class Sensors : MonoBehaviour
{
    // Reichweite der Sensoren
    [SerializeField] private float sensorRange;

    // Distanz die gemessen wird vom Sensor
    [SerializeField] private Text n_Value;
    private float n_Distance;

    [SerializeField] private Text ne_Value;
    private float ne_Distance;

    [SerializeField] private Text e_Value;
    private float e_Distance;

    [SerializeField] private Text se_Value;
    private float se_Distance;

    [SerializeField] private Text s_Value;
    private float s_Distance;

    [SerializeField] private Text sw_Value;
    private float sw_Distance;

    [SerializeField] private Text w_Value;
    private float w_Distance;

    [SerializeField] private Text nw_Value;
    private float nw_Distance;

    private Vector3 robotPosition;
    private Quaternion robotRotation;

    private void FixedUpdate()
    {
        // Rotation und Position soll jeden Frame abgefragt werden
        robotPosition = this.transform.localPosition;
        robotRotation = this.transform.localRotation;

        // Sensoren die benutzt werden sollen
        NE_Sensor();
        N_Sensor();
        NW_Sensor();
        W_Sensor();
        SW_Sensor();
        S_Sensor();
        SE_Sensor();
        E_Sensor();
    }
    #endregion

    #region Methoden
    private void NE_Sensor()
    {
        // Erstelle ein Strahl in eine bestimmte Richtung vom Roboter aus, beachte dabei die Rotation des Roboters
        Ray ray_NE = new Ray(robotPosition, robotRotation * new Vector3(1, 0, 1));
        RaycastHit hitInfo_NE;

        if (Physics.Raycast(ray_NE, out hitInfo_NE, sensorRange))
        {
            // Wenn der Strahl etwas trifft, ist er rot und der Sensor gibt die Reichweite aus, wobei er mindestens 0.1 m anzeigt
            ne_Distance = Vector3.Distance(ray_NE.origin, hitInfo_NE.point) - 0.15f;

            Debug.DrawLine(ray_NE.origin, hitInfo_NE.point, Color.red);
            ne_Value.text = "" + ne_Distance;
        }
        else
        {
            // Wenn der Strahl in seiner Reichweite nichts trifft wird er grün
            Debug.DrawLine(ray_NE.origin, ray_NE.origin + ray_NE.direction * sensorRange, Color.green);
            ne_Value.text = "--";

            ne_Distance = sensorRange;
        }
    }

    private void N_Sensor()
    {
        Ray ray_N = new Ray(robotPosition, robotRotation * new Vector3(0, 0, 1));
        RaycastHit hitInfo_N;

        if (Physics.Raycast(ray_N, out hitInfo_N, sensorRange))
        {
            n_Distance = Vector3.Distance(ray_N.origin, hitInfo_N.point) - 0.15f;

            Debug.DrawLine(ray_N.origin, hitInfo_N.point, Color.red);
            n_Value.text = "" + n_Distance;
        }
        else
        {
            Debug.DrawLine(ray_N.origin, ray_N.origin + ray_N.direction * sensorRange, Color.green);
            n_Value.text = "--";

            n_Distance = sensorRange;
        }
    }

    private void NW_Sensor()
    {
        Ray ray_NW = new Ray(robotPosition, robotRotation * new Vector3(-1, 0, 1));
        RaycastHit hitInfo_NW;

        if (Physics.Raycast(ray_NW, out hitInfo_NW, sensorRange))
        {
            nw_Distance = Vector3.Distance(ray_NW.origin, hitInfo_NW.point) - 0.15f;

            Debug.DrawLine(ray_NW.origin, hitInfo_NW.point, Color.red);
            nw_Value.text = "" + nw_Distance;
        }
        else
        {
            Debug.DrawLine(ray_NW.origin, ray_NW.origin + ray_NW.direction * sensorRange, Color.green);
            nw_Value.text = "--";

            nw_Distance = sensorRange;
        }
    }

    private void W_Sensor()
    {
        Ray ray_W = new Ray(robotPosition, robotRotation * new Vector3(-1, 0, 0));
        RaycastHit hitInfo_W;

        if (Physics.Raycast(ray_W, out hitInfo_W, sensorRange))
        {
            w_Distance = Vector3.Distance(ray_W.origin, hitInfo_W.point) - 0.15f;

            Debug.DrawLine(ray_W.origin, hitInfo_W.point, Color.red);
            w_Value.text = "" + w_Distance;
        }
        else
        {
            Debug.DrawLine(ray_W.origin, ray_W.origin + ray_W.direction * sensorRange, Color.green);
            w_Value.text = "--";

            w_Distance = sensorRange;
        }
    }

    private void SW_Sensor()
    {
        Ray ray_SW = new Ray(robotPosition, robotRotation * new Vector3(-1, 0, -1));
        RaycastHit hitInfo_SW;

        if (Physics.Raycast(ray_SW, out hitInfo_SW, sensorRange))
        {
            sw_Distance = Vector3.Distance(ray_SW.origin, hitInfo_SW.point) - 0.15f;

            Debug.DrawLine(ray_SW.origin, hitInfo_SW.point, Color.red);
            sw_Value.text = "" + sw_Distance;
        }
        else
        {
            Debug.DrawLine(ray_SW.origin, ray_SW.origin + ray_SW.direction * sensorRange, Color.green);
            sw_Value.text = "--";

            sw_Distance = sensorRange;
        }
    }

    private void S_Sensor()
    {
        Ray ray_S = new Ray(robotPosition, robotRotation * new Vector3(0, 0, -1));
        RaycastHit hitInfo_S;

        if (Physics.Raycast(ray_S, out hitInfo_S, sensorRange))
        {
            s_Distance = Vector3.Distance(ray_S.origin, hitInfo_S.point) - 0.15f;

            Debug.DrawLine(ray_S.origin, hitInfo_S.point, Color.red);
            s_Value.text = "" + s_Distance;
        }
        else
        {
            Debug.DrawLine(ray_S.origin, ray_S.origin + ray_S.direction * sensorRange, Color.green);
            s_Value.text = "--";

            s_Distance = sensorRange;
        }
    }

    private void SE_Sensor()
    {
        Ray ray_SE = new Ray(robotPosition, robotRotation * new Vector3(1, 0, -1));
        RaycastHit hitInfo_SE;

        if (Physics.Raycast(ray_SE, out hitInfo_SE, sensorRange))
        {
            se_Distance = Vector3.Distance(ray_SE.origin, hitInfo_SE.point) - 0.15f;

            Debug.DrawLine(ray_SE.origin, hitInfo_SE.point, Color.red);
            se_Value.text = "" + se_Distance;
        }
        else
        {
            Debug.DrawLine(ray_SE.origin, ray_SE.origin + ray_SE.direction * sensorRange, Color.green);
            se_Value.text = "--";

            se_Distance = sensorRange;
        }
    }

    private void E_Sensor()
    {
        Ray ray_E = new Ray(robotPosition, robotRotation * new Vector3(1, 0, 0));
        RaycastHit hitInfo_E;

        if (Physics.Raycast(ray_E, out hitInfo_E, sensorRange))
        {
            e_Distance = Vector3.Distance(ray_E.origin, hitInfo_E.point) - 0.15f;

            Debug.DrawLine(ray_E.origin, hitInfo_E.point, Color.red);
            e_Value.text = "" + e_Distance;
        }
        else
        {
            Debug.DrawLine(ray_E.origin, ray_E.origin + ray_E.direction * sensorRange, Color.green);
            e_Value.text = "--";

            e_Distance = sensorRange;
        }
    }
    #endregion

    #region Setter Getter
    public float N_Distance { get => n_Distance; set => n_Distance = value; }
    public float NE_Distance { get => ne_Distance; set => ne_Distance = value; }
    public float E_Distance { get => e_Distance; set => e_Distance = value; }
    public float SE_Distance { get => se_Distance; set => se_Distance = value; }
    public float S_Distance { get => s_Distance; set => s_Distance = value; }
    public float SW_Distance { get => sw_Distance; set => sw_Distance = value; }
    public float W_Distance { get => w_Distance; set => w_Distance = value; }
    public float NW_Distance { get => nw_Distance; set => nw_Distance = value; }
}
#endregion
